# vim_hl

Vim HL plugin to be able to highlight regular expressions both for buffers (:HL) and windows (:HLW) and to
"highshadow" (:HS) regular expressions

The idea behind the plugin is to easily highlight a set of regular expressions without worrying too
much for the color, without colliding with match 2match... and also friendly with the syntax of file 
types.

## usage

The usage is pretty simple (in command mode):

The plugin works both with buffers and with windows, when using the windows you can have different
highlight groups for the same buffer in different windows, but when switching buffers the highlight
will be maintained (even if the file type is not the same)

The command HL is for buffers and HLW is for windows, but they work almost the same.

for buffers: 

```vi
" this will set a highlight on the index
:HL [0-41] <regexp>
" this will set a highlight using automatic coloring
:HL <regexp>
" this will add an expression to the last highlight
:HLAdd <regexp>
" this will add an expression to a given highlight
:HLAdd [0-41] <regexp>


" this will remove a given highlight
:HL [0-41]
" this will remove all the highlights (and "highshadows")
:HL

" for buffers shadowing is also enabled (not for windows)

" shadow with the default grey color (2)
:HS <regexp>

" shadow with a given gray tone, or white (0 - 3 -> white - darkgrey)
:HS 3 <regexp>

" shadows can be removed with both
:HS

" or
:HL

" but HS will not remove highlights
```

for windows (is the same but with HLW): 

```vi
" this will set a highlight on the index
:HLW [0-7] <regexp>
" this will set a highlight using automatic coloring
:HLW <regexp>
" this will remove a given highlight
:HLW [0-7]
" this will remove all the highlights from the window
:HLW
```

Colors:

Right now there are 42 colors (0-41) but I want to improve them.

This extension assumes vim supports `truecolor` on the terminal and that the following option is set:
```vi
:set termguicolors
```

NOTE: you should really add this to your .vimrc

NOTE2: tmux by default does not support true colors, I had to put this on .tmux.conf and with the terminals I use
it does work.

```
set -g default-terminal "${TERM}"

set -sa terminal-overrides ",xterm*:Tc"
set -ga terminal-overrides ",xterm*:Tc"

```



### Creating a command to highlight the word under the cursor


```vi
# in this case we assign <leader>* so highlight word is similar to search word
nnoremap <leader>* :HL \<<c-r>=expand("<cword>")<cr>\><cr>
```

### save buffer highlight

For now there is no window hightlight save option, but you can save the current
buffer's hightlight in a file that afterwards can be source'd (including HS).

```vi 
" first you save it
:HLSave "filename"

" then you can read it at any point
:source "filename"
```


## examples

```vi
" Highlight different things: the word "mikel", text starting with "TODO:"...
:HL 0 mikel
:HL 1 TODO:.*
:HL 2 rx:.*
:HL 3 tx:.*
:HLAdd 0 something
" Highlight notes on the current window no matter the buffer
:HLW 0 NOTE:.*

" Remove highlight of the words mikel and something
:HL 0
" Remove all the highlights of the buffer (but the window still has the "NOTE:" one)
:HL
```

## installation

With vim-plug add this to your .vimrc:
```vi 
Plug 'https://gitlab.com/mikelgarai/vim_hl.git'
```

For vim native package system, adding to the package vim_hl (you can decide any other package name):

```sh
$ mkdir -p ~/.vim/pack/vim_hl/start/
$ git clone https://gitlab.com/mikelgarai/vim_hl.git ~/.vim/pack/vim_hl/start/vim_hl
```
## TODO

* ~~Aside from window and buffer HL add a global HL (HLG or simply HL) for all the windows and buffers~~
* Make :TOHtml work with highlight.. or add this functionality by any other method.
* ~~Add the option to automatically enumerate highlights with the first free slot~~
    * ~~(or) Default highlight group "0" if none specified~~
* Save/Restore the current highlights to a file
  * Add option to save window highlights
  * ¿ Maybe remove HLW ? I never use it
* ~~Add more groups~~
* Allow configuration of the style of each group
    * (while I decide if I'll do this) Decent color palette (now is only okayish)
* Option to set the current autoindex manually
* Option to print the current HL/HS groups
* ~~¿ Temporary highlights ?~~
