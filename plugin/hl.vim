function! HL_define_hl()
	highlight hl_def_0  guifg=#7fff7f gui=bold cterm=bold ctermfg=green
	highlight hl_def_1  guifg=#ff7f7f gui=bold cterm=bold ctermfg=red
	highlight hl_def_2  guifg=#7f7fff gui=bold cterm=bold ctermfg=blue
	highlight hl_def_3  guifg=#bf7fff gui=bold cterm=bold ctermfg=yellow
	highlight hl_def_4  guifg=#bfff7f gui=bold cterm=bold ctermfg=magenta
	highlight hl_def_5  guifg=#7fffff gui=bold cterm=bold ctermfg=cyan
	highlight hl_def_6  guifg=#ff7fff gui=bold cterm=bold ctermfg=lightmagenta
	highlight hl_def_7  guifg=#ffbf7f gui=bold cterm=bold ctermfg=lightgreen
	highlight hl_def_8  guifg=#ffff7f gui=bold cterm=bold ctermfg=lightred
	highlight hl_def_9  guifg=#ff7fbf gui=bold cterm=bold ctermfg=lightblue
	highlight hl_def_10 guifg=#7fffbf gui=bold cterm=bold ctermfg=lightcyan
	highlight hl_def_11 guifg=#7fbfff gui=bold cterm=bold ctermfg=lightyellow
	highlight hl_def_12 guifg=#00ff00 gui=bold cterm=bold ctermfg=green
	highlight hl_def_13 guifg=#ff0000 gui=bold cterm=bold ctermfg=red
	highlight hl_def_14 guifg=#0000ff gui=bold cterm=bold ctermfg=blue
	highlight hl_def_15 guifg=#7f00ff gui=bold cterm=bold ctermfg=yellow
	highlight hl_def_16 guifg=#7fff00 gui=bold cterm=bold ctermfg=magenta
	highlight hl_def_17 guifg=#00ffff gui=bold cterm=bold ctermfg=cyan
	highlight hl_def_18 guifg=#ff00ff gui=bold cterm=bold ctermfg=lightmagenta
	highlight hl_def_19 guifg=#ff7f00 gui=bold cterm=bold ctermfg=lightgreen
	highlight hl_def_20 guifg=#ffff00 gui=bold cterm=bold ctermfg=lightred
	highlight hl_def_21 guifg=#ff007f gui=bold cterm=bold ctermfg=lightblue
	highlight hl_def_22 guifg=#00ff7f gui=bold cterm=bold ctermfg=lightcyan
	highlight hl_def_23 guifg=#007fff gui=bold cterm=bold ctermfg=lightyellow
	highlight hl_def_24 guifg=#7fff9f gui=bold cterm=bold ctermfg=green
	highlight hl_def_25 guifg=#ff9f7f gui=bold cterm=bold ctermfg=red
	highlight hl_def_26 guifg=#9f7fff gui=bold cterm=bold ctermfg=blue
	highlight hl_def_27 guifg=#df7fff gui=bold cterm=bold ctermfg=yellow
	highlight hl_def_28 guifg=#9fff7f gui=bold cterm=bold ctermfg=magenta
	highlight hl_def_29 guifg=#7fdfff gui=bold cterm=bold ctermfg=cyan
	highlight hl_def_30 guifg=#ff7fdf gui=bold cterm=bold ctermfg=lightmagenta
	highlight hl_def_31 guifg=#ffdf7f gui=bold cterm=bold ctermfg=lightgreen
	highlight hl_def_32 guifg=#dfff7f gui=bold cterm=bold ctermfg=lightred
	highlight hl_def_33 guifg=#ff7f9f gui=bold cterm=bold ctermfg=lightblue
	highlight hl_def_34 guifg=#7fffdf gui=bold cterm=bold ctermfg=lightcyan
	highlight hl_def_35 guifg=#7f9fff gui=bold cterm=bold ctermfg=lightyellow
	highlight hl_def_36 guifg=#00ff3f gui=bold cterm=bold ctermfg=green
	highlight hl_def_37 guifg=#ff3f00 gui=bold cterm=bold ctermfg=red
	highlight hl_def_38 guifg=#3f00ff gui=bold cterm=bold ctermfg=blue
	highlight hl_def_39 guifg=#bf00ff gui=bold cterm=bold ctermfg=yellow
	highlight hl_def_40 guifg=#3fff00 gui=bold cterm=bold ctermfg=magenta
	highlight hl_def_41 guifg=#00bfff gui=bold cterm=bold ctermfg=cyan
	highlight hl_def_42 guifg=#ff00bf gui=bold cterm=bold ctermfg=lightmagenta
	highlight hl_def_43 guifg=#ffbf00 gui=bold cterm=bold ctermfg=lightgreen
	highlight hl_def_44 guifg=#bfff00 gui=bold cterm=bold ctermfg=lightred
	highlight hl_def_45 guifg=#ff003f gui=bold cterm=bold ctermfg=lightblue
	highlight hl_def_46 guifg=#00ffbf gui=bold cterm=bold ctermfg=lightcyan
	highlight hl_def_47 guifg=#003fff gui=bold cterm=bold ctermfg=lightyellow


	highlight hl_sh_def_0 guifg=#ffffff gui=bold cterm=bold ctermfg=white
	highlight hl_sh_def_1 guifg=#7f7f7f gui=bold cterm=bold ctermfg=lightgray
	highlight hl_sh_def_2 guifg=#3f3f3f gui=bold cterm=bold ctermfg=gray
	highlight hl_sh_def_3 guifg=#1f1f1f gui=bold cterm=bold ctermfg=darkgray
endfunction

let s:hl_num = 48
let s:hs_num = 4 

function! Create_buffer_arrays()
	if !exists("b:matches")
		let b:matches = []
		let b:auto_index = 0
	endif
	if !exists("b:hs_matches")
		let b:hs_matches = []
	endif
endfunction

function! HL_update()
	" create the window buffer matches
	if !exists("w:b_matches")
		let w:b_matches = []
	endif
	let i=0
	while i < len(w:b_matches)
		call matchdelete(w:b_matches[i])
		let i = i + 1
	endwhile
	let w:b_matches = []
	if !exists("b:matches") && !exists("b:hs_matches")
		return
	endif
	call Create_buffer_arrays()
	call HL_define_hl()
	let i=0
	while i < len(b:matches)
		call add(w:b_matches, matchadd("hl_def_".b:matches[i][0], b:matches[i][1], -1))
		let i = i + 1
	endwhile
	let i=0
	while i < len(b:hs_matches)
		call add(w:b_matches, matchadd("hl_sh_def_".b:hs_matches[i][0], b:hs_matches[i][1], -1))
		let i = i + 1
	endwhile
endfunction

au BufEnter * call HL_update()
au WinEnter * call HL_update()

function! HLW(...)
	if !exists("w:matches")
		let w:matches = []
		let w:auto_index = 0
	endif
	let id = a:1
	let exp = a:2

	if id == -1 && exp != ""
		let id = w:auto_index
		if w:auto_index < s:hl_num - 1
			let w:auto_index += 1
		else
			let w:auto_index = 0
		endif
	endif

	if id == -1
		let i=0
		while len(w:matches) > 0
			call matchdelete(w:matches[0][1])
			unlet w:matches[0]
		endwhile
		let w:auto_index = 0
	else
		let i = 0
		while i < len(w:matches)
			if w:matches[i][0] == id
				call matchdelete(w:matches[i][1])
				unlet w:matches[i]
			else
				let i = i + 1
			endif
		endwhile
		if exp != ""
			call HL_define_hl()
			call add(w:matches, [id, matchadd("hl_def_".id, exp, -1)])
		endif
	endif
endfunction

function! HL(...)
	call Create_buffer_arrays()
	let id = a:1
	let exp = a:2
		
	if id == -1 && exp != ""
		let id = b:auto_index
		if b:auto_index < s:hl_num - 1
			let b:auto_index += 1
		else
			let b:auto_index = 0
		endif
	endif
	
	if id == -1
		let b:matches = []
		let b:auto_index = 0
		let b:hs_matches = []
	else
		let i=0
		while i < len(b:matches)
			if b:matches[i][0] == id
				unlet b:matches[i]
			else
				let i = i + 1
			endif
		endwhile
		if exp != ""
			call add(b:matches, [id, exp])
		endif
	endif
	call HL_update()
endfunction

function! HL_add(...)
	call Create_buffer_arrays()
	let id = a:1
	let exp = a:2

	if id == -1 && exp != ""
		if (len(b:matches) == 0)
			let id = 0
		else
			let id = b:matches[len(b:matches)-1][0]
		endif
	endif

	if exp == ""
		return
	endif

	if id == -1
		let b:matches = []
		let b:auto_index = 0
		let b:hs_matches = []
	else
		let i=0
		while i < len(b:matches)
			if b:matches[i][0] == id
				let exp = b:matches[i][1] . "\\|" . exp
				unlet b:matches[i]
			else
				let i = i + 1
			endif
		endwhile
		call add(b:matches, [id, exp])
	endif
	call HL_update()
endfunction

function! HS(...)
	call Create_buffer_arrays()
	let id = a:1
	let exp = a:2
	
	if id == -1 && exp != ""
		let id = 2
	endif
	
	if id == -1
		let b:hs_matches = []
	else
		let i=0
		while i < len(b:hs_matches)
			if b:hs_matches[i][0] == id
				unlet b:hs_matches[i]
			else
				let i = i + 1
			endif
		endwhile
		if exp != ""
			call add(b:hs_matches, [id, exp])
		endif
	endif
	call HL_update()
endfunction

function! HS_add(...)
	call Create_buffer_arrays()
	let id = a:1
	let exp = a:2

	if id == -1 && exp != ""
		if (len(b:hs_matches) == 0)
			let id = 0
		else
			let id = b:hs_matches[len(b:hs_matches)-1][0]
		endif
	endif

	if exp == ""
		return
	endif

	if id == -1
		let b:hs_matches = []
	else
		let i=0
		while i < len(b:hs_matches)
			if b:hs_matches[i][0] == id
				let exp = b:hs_matches[i][1] . "\\|" . exp
				unlet b:hs_matches[i]
			else
				let i = i + 1
			endif
		endwhile
		call add(b:hs_matches, [id, exp])
	endif
	call HL_update()
endfunction

function! HL_extract_id(...)
	if a:1 == ""
		return -1
	endif
	let pos = match(a:1, " ")
	let tmp = str2nr(a:1)
	if pos == -1 || a:1 !~ '^[0-9]\{1,2\} .*' || tmp < 0 || tmp >= s:hl_num
		return -1
	endif
	return tmp
endfunction

function! HS_extract_id(...)
	let id = HL_extract_id(a:1)
	if id >= s:hs_num
		return -1
	endif
	return id
endfunction

function! HL_extract_expr(...)
	if a:2 == -1
		return a:1
	endif
	let pos = match(a:1, " ")
	if pos == -1
		return ""
	endif
	return strpart(a:1, pos+1)
endfunction

function! HS_command(...)
	let id = HS_extract_id(a:1)
	let exp = HL_extract_expr(a:1, id)
	call HS(id, exp)
endfunction

function! HS_add_command(...)
	let id = HS_extract_id(a:1)
	let exp = HL_extract_expr(a:1, id)
	call HS_add(id, exp)
endfunction

function! HL_command(...)
	let id = HL_extract_id(a:1)
	let exp = HL_extract_expr(a:1, id)
	call HL(id, exp)
endfunction

function! HL_add_command(...)
	let id = HL_extract_id(a:1)
	let exp = HL_extract_expr(a:1, id)
	call HL_add(id, exp)
endfunction

function! HLW_command(...)
	let id = HL_extract_id(a:1)
	let exp = HL_extract_expr(a:1, id)
	call HLW(id, exp)
endfunction

function! HL_save(...)
	let l:hl_rules = ["HL"]

	let i=0
	while i < len(b:matches)
		call add(l:hl_rules, "HL ".b:matches[i][0]." ".b:matches[i][1] )
		let i = i + 1
	endwhile
	call add(l:hl_rules, "HS")
	let i=0
	while i < len(b:hs_matches)
		call add(l:hl_rules, "HS ".b:hs_matches[i][0]." ".b:hs_matches[i][1] )
		let i = i + 1
	endwhile
	call writefile(l:hl_rules, expand(a:1))
endfunction

command! -nargs=* -complete=command HSAdd call HS_add_command(<q-args>)
command! -nargs=* -complete=command HS call HS_command(<q-args>)

command! -nargs=* -complete=command HLAdd call HL_add_command(<q-args>)
command! -nargs=* -complete=command HL call HL_command(<q-args>)

command! -nargs=* -complete=command HLW call HLW_command(<q-args>)

command! -nargs=1 -complete=command HLSave call HL_save(<q-args>)

